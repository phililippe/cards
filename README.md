
# SETUP

## PREREQUISITES

- Node JS 10+
- PostgreSQL 9+

## INSTALL DEPENDANCES

```console
$ cd [path of the project]
$ npm install

```

## CREATE DB

```console
$ create_db cards

```

## SETUP CONFIG

```console
$ cp .env.example .env

```

Then edit your .env with your local settings. 

## SEED THE DB

```console
$ npm run seed

```

All should be OK now.

You can also import the provided Postman collection in order to test.

# USE IT

```console
$ npm run start:dev

```

This will start a local server on port 3002.

You can use it along with the provided Postman collection.

## MAIN ROUTES

Your calls must include a X-User-UUID header or a X-Company-UUID header.
Sample available values: 

- X-Company-UUID: c1160501-d4e5-491e-b4e9-aa483721f150
- X-User-UUID: 0365c988-5999-468a-88fa-525acf01c19a

### List wallets

```console
$ curl --location --request GET 'http://localhost:3002/wallets/' \
--header 'X-Company-UUID: [Company UUID]'
```

### Create wallet

```console
$ curl --location --request POST 'http://localhost:3002/wallets/' \
--header 'X-Company-UUID: [Company UUID]' \
--header 'Content-Type: application/json' \
--data-raw '{
	"name":"[a name]",
	"currency": "[EUR, USD or GBP]"
}'
```

### Transfer between wallets

```console
$ curl --location --request POST 'http://localhost:3002/wallets/[Source Wallet UUID]/send/' \
--header 'X-Company-UUID: [Source Company UUID]' \
--header 'Content-Type: application/json' \
--data-raw '{
	"amount": [Amount in source currency],
	"toWalletUUID": "[Destination Company UUID]"
}'
```

In case the 2 wallet don't have the same currency, exchange rate will be applied and a fee will be collected. 2 transfers will be issued: one for transaction, one for the fees.

### List cards

```console
$ curl --location --request GET 'http://localhost:3002/cards/' \
--header 'X-User-UUID: [User UUID]'
```

### Create card

```console
$ curl --location --request POST 'http://localhost:3002/cards/' \
--header 'X-User-UUID: [User UUID]' \
--header 'X-Company-UUID: [Company UUID]' \
--header 'Content-Type: application/json' \
--data-raw '{
	"name":"[a name]",
	"walletUUID": "[parent wallet UUID]"
}'
```

### Load/unload card

A positive amount will load the card, while a negative amount will unload it.

```console
$ curl --location --request PATCH 'http://localhost:3002/cards/[Card UUID]/fund' \
--header 'X-User-UUID: [User UUID]' \
--header 'Content-Type: application/json' \
--data-raw '{
	"amount": [amount to (un)load]
}'

```

### Block/unblock card

Works like a switch. If the card is active, it will be blocked. If it is blocked, it will be unblocked.

```console
$ curl --location --request PATCH 'http://localhost:3002/cards/[Card UUID]/block' \
--header 'X-User-UUID: [User UUID]' \
--header 'Content-Type: application/json'
```

# IMPROVEMENTS, TO DO LIST

At this stage, lot of improvements can be done:

- refactoring: making the code more testable, all the 'transfer' logic could be factored in a single method, create directories and smaller files

- tests (unit, integration). Jest has been added as a dependency but it's not used. We should remove instances creation from seeding (except for master wallets) as they should be mocked in tests

- all authentication management could also been moved in separate methods and make use of the request chaining capabilities of Express (check permission, if ok, go to next method handling the custom logic)

- serialization should be improved: with better type management, and filtering values we want to send to client


require('dotenv').config();
const Sequelize = require('sequelize');

const isProduction = process.env.NODE_ENV === 'production';
const allowedCurrencies = ['EUR', 'USD', 'GPB'];

module.exports = { allowedCurrencies, isProduction };
const { Sequelize, Model, DataTypes } = require('sequelize');
const { allowedCurrencies } = require('./config');

const sequelize = new Sequelize(process.env.DB_DATABASE, process.env.DB_USER, process.env.DB_PASSWORD, {
    dialect: 'postgres'
  })

class Wallet extends Model {}
Wallet.init({
    uuid: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV1,
        primaryKey: true
    },
    name: DataTypes.STRING,
    currency: {
        type: DataTypes.ENUM(...allowedCurrencies),
        allowNull: false
    },
    balance: DataTypes.DECIMAL(10, 2),  // On some use cases, we could use more decimal (eg. for cryptos)
    isMasterWallet: {
        field: 'is_master_wallet',
        type: DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: false
    },
    // Here, in a real case, we should manage a foreign key through a belongsTo() relation on Organization
    companyUUID: {
        field: 'company_uuid',
        allowNull: false,
        type: DataTypes.UUID
    },
    createdAt: {
        field: 'created_at',
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: DataTypes.NOW
    },
    updatedAt: {
        field: 'updated_at',
        allowNull: false,
        type: Sequelize.DATE
    }

}, { sequelize, modelName: 'wallet' });

class Card extends Model {}
Card.cardStatuses = ['active', 'blocked', 'expired'];

Card.init({
    uuid: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV1,
        primaryKey: true
    },
    name: DataTypes.STRING,
    currency: {
        type: DataTypes.ENUM(...allowedCurrencies),
        allowNull: false
    },
    status: {
        type: DataTypes.ENUM(...Card.cardStatuses),
        allowNull: false,
        defaultValue: 'active'
    },
    number: {
        field: 'card_number',
        allowNull: false,
        type: DataTypes.DECIMAL(16, 0)        
    },
    ccv: {
        field: 'card_ccv',
        allowNull: false,
        type: DataTypes.DECIMAL(3, 0)        
    },
    balance: DataTypes.DECIMAL(10, 2),  // On some use cases, we could use more decimal (eg. for cryptos)
    // Here, in a real case, we should manage a foreign key through a belongsTo() relation on User
    ownerUUID: {
        field: 'owner_uuid',
        allowNull: false,
        type: DataTypes.UUID
    },
    createdAt: {
        field: 'created_at',
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: DataTypes.NOW
    },
    updatedAt: {
        field: 'updated_at',
        allowNull: false,
        type: Sequelize.DATE
    },
    expireAt: {
        field: 'expire_at',
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: () => {
            const today = new Date();
            return new Date(today.setMonth(today.getMonth()+1));
          }
    }
}, { sequelize, modelName: 'card' });

// Wallet.hasMany(Card, {as: 'cards'});
Card.belongsTo(Wallet, {as: 'wallet', onDelete: 'cascade', foreignKey: {name: 'walletUUID', field: 'wallet_uuid', foreignKeyConstraint: true, allowNull: false}});


class Transfer extends Model {}
Transfer.entitiesTypes = ['wallet', 'card'];
Transfer.init({
    uuid: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV1,
        primaryKey: true
    },
    amount: DataTypes.DECIMAL(10, 2),  // On some use cases, we could use more decimal (eg. for cryptos)
    fromUUID: {
        field: 'from_uuid',
        allowNull: false,
        type: DataTypes.UUID
    },
    fromCurrency: {
        field: 'from_currency',
        type: DataTypes.ENUM(...allowedCurrencies),
        allowNull: false
    },
    fromEntityType: {
        field: 'from_entity_type',
        type: DataTypes.ENUM(...Transfer.entitiesTypes),
        allowNull: false
    },
    conversionRate: {
        field: 'conversion_rate',
        allowNull: false,
        defaultValue: 1.0,
        type: DataTypes.FLOAT
    },
    conversionFee: {
        field: 'conversion_fee',
        allowNull: true,
        type: DataTypes.DECIMAL(10, 2)
    },
    toUUID: {
        field: 'to_uuid',
        allowNull: false,
        type: DataTypes.UUID
    },
    toCurrency: {
        field: 'to_currency',
        type: DataTypes.ENUM(...allowedCurrencies),
        allowNull: false
    },
    toEntityType: {
        field: 'to_entity_type',
        type: DataTypes.ENUM(...Transfer.entitiesTypes),
        allowNull: false
    },
    // status: {initiated, confirmed, ... } In a distributed system we should considere those statuses
    // Here, in a real case, we should manage a foreign key through a belongsTo() relation on User
    initiatedAt: {
        field: 'initiated_at',
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: DataTypes.NOW
    },
}, { sequelize, modelName: 'transfer' });

module.exports = { sequelize, Wallet, Card, Transfer };
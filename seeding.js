const { sequelize } = require('./config');
const { Wallet, Card, Transfer } = require('./models.js');

const masterOrganisationUUID = 'b12a4844-deac-4167-9134-7efe84dc77b5';
const sampleUserUUID = '0365c988-5999-468a-88fa-525acf01c19a';
const sampleOrganizationUUID = 'c1160501-d4e5-491e-b4e9-aa483721f150';

(async () => {

    await Wallet.sync({ force: true });

    Wallet.create({
        name: 'Master wallet EUR',
        currency: 'EUR',
        balance: 100000.00,
        isMasterWallet: true,
        companyUUID: masterOrganisationUUID
    });
    Wallet.create({
        name: 'Master wallet USD',
        currency: 'USD',
        balance: 100000.00,
        isMasterWallet: true,
        companyUUID: masterOrganisationUUID
    });
    Wallet.create({
        name: 'Master wallet GPB',
        currency: 'GPB',
        balance: 100000.00,
        isMasterWallet: true,
        companyUUID: masterOrganisationUUID
    });
    const walletEUR = await Wallet.create({
        name: 'User wallet EUR',
        currency: 'EUR',
        balance: 1000.00,
        isMasterWallet: false,
        companyUUID: sampleOrganizationUUID
    });
    const walletUSD = await Wallet.create({
        name: 'User wallet USD',
        currency: 'USD',
        balance: 1000.00,
        isMasterWallet: false,
        companyUUID: sampleOrganizationUUID
    });

    await Card.sync({ force: true });

    await Card.create({
        name: 'User EUR Wallet card',
        currency: 'EUR',
        balance: 100.00,
        number:'1234567890123456',
        ccv: '123',
        walletUUID: walletEUR.uuid,
        ownerUUID: sampleUserUUID,
    });

    await Card.create({
        name: 'User USD Wallet card',
        currency: 'USD',
        balance: 100.00,
        number:'5678901234567890',
        ccv: '765',
        walletUUID: walletUSD.uuid,
        ownerUUID: sampleUserUUID,
    });

    await Transfer.sync({ force: true });

})();
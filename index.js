const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const fetch = require('node-fetch');
const { isProduction } = require('./config');
const { sequelize, Wallet, Card, Transfer } = require('./models.js');

const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

const origin = {
    // Will fail if isProduction and not process.env.ORIGIN_URL
    // ... but better fail fast
    origin: isProduction ? process.env.ORIGIN_URL : '*',
  }
  
app.use(cors(origin));

const fetchCurrentRate = async (fromCurrency, toCurrency) => {
    const url = `http://data.fixer.io/api/latest?access_key=${process.env.FIXERIO_API_KEY}&base=${fromCurrency}&symbols=${toCurrency}`;

    try {
        const response = await fetch(url);
        const data = await response.json();

        if(data.error)
            throw data.error;
        if(data.success === true) {
            return parseFloat(data.rates[toCurrency]);
        }
        throw "Unhandled error";
    } catch(error) {
        console.error(error);
        throw error;
    }
};


const addWallet = (request, response) => {
    const { currency, name } = request.body;
    const companyUUID = request.header('X-Company-UUID');

    if(!companyUUID) {
        response.status(401).json({status: 'error', data: null, message: 'Not allowed'});
    }

    Wallet
        .create({currency, name, companyUUID, balance: 100000.00})
        .then(wallet => {
            response.status(201).json({status: 'success', data: wallet.toJSON()});
        })
        .catch(error => {
            // Sending the raw error is may a little too much (and is not secure). 
            // Better send a more appropriate message and log the error.
            response.status(500).json({status: 'error', data: null, message: error});
        });
}

const getWallets = (request, response) => {
    const companyUUID = request.header('X-Company-UUID');

    if(!companyUUID) {
        response.status(401).json({status: 'error', data: null, message: 'Not allowed'});
    }

    Wallet
        .findAll({
            where: {
                companyUUID
            }
        })
        .then(wallets => {
            const results = wallets.map(wallet => wallet.toJSON());
            response.status(200).json({status: 'success', data: results});
        })
        .catch(error => {
            // Sending the raw error is may a little too much (and is not secure). 
            // Better send a more appropriate message and log the error.
            response.status(500).json({status: 'error', data: null, message: error});
        });
}

const addCard = (request, response) => {
    const { walletUUID, name } = request.body;
    const ownerUUID = request.header('X-User-UUID');

    if(!ownerUUID) {
        response.status(401).json({status: 'error', data: null, message: 'Not allowed'});
    }

    if(!walletUUID) {
        response.status(400).json({status: 'error', data: null, message: 'Needs a walletUUID'});
    }

    Wallet.findByPk(walletUUID)
        .then(wallet => {

            function makeid(len) {
                let result = '';
                const characters = '0123456789';
                for ( var i = 0; i < len; i++ ) {
                   result += characters.charAt(Math.floor(Math.random() * characters.length));
                }
                return result;
            }
            const number = parseInt(makeid(16), 10);
            const ccv = parseInt(makeid(3), 10);

            Card
            .create({currency: wallet.currency, name, ownerUUID, number, ccv, walletUUID: wallet.uuid, balance: 0.00})
            .then(card => {
                response.status(201).json({status: 'success', data: card.toJSON()});
            })
            .catch(error => {
                // Sending the raw error is may a little too much (and is not secure). 
                // Better send a more appropriate message and log the error.
                response.status(500).json({status: 'error', data: null, message: error});
            });
        })
        .catch(error => {
            // In fact, many cases to check here. May be the wallet doesn't exists or 
            // may be the user is not allowed...
            response.status(500).json({status: 'error', data: null, message: error});
        });       
}

const getCards = (request, response) => {
    const ownerUUID = request.header('X-User-UUID');

    if(!ownerUUID) {
        response.status(401).json({status: 'error', data: null, message: 'Not allowed'});
    }

    Card
        .findAll({
            where: {
                ownerUUID
            }
        })
        .then(cards => {
            const results = cards.map(card => card.toJSON());
            response.status(200).json({status: 'success', data: results});
        })
        .catch(error => {
            // Sending the raw error is may a little too much (and is not secure). 
            // Better send a more appropriate message and log the error.
            response.status(500).json({status: 'error', data: null, message: error});
        });
}

const fundCard = (request, response) => {
    const ownerUUID = request.header('X-User-UUID');
    const cardUUID = request.params.cardUUID;
    const amount = parseFloat(request.body.amount);

    if(!ownerUUID) {
        response.status(401).json({status: 'error', data: null, message: 'Not allowed'});
    }

    if(!amount) {
        response.status(400).json({status: 'error', data: null, message: 'Needs an amount'});
    }

    Card.findByPk(cardUUID)
        .then(async (card) => {

            if(card.ownerUUID != ownerUUID) {
                response.status(401).json({status: 'error', data: null, message: 'Not allowed'});
            }
            if(card.status != 'active') {
                response.status(401).json({status: 'error', data: null, message: 'Not allowed'});
            }
            if(card.expireAt < new Date()) {
                card.status = 'expired';
                await card.save();
                response.status(401).json({status: 'error', data: null, message: 'Not allowed'});
            }
            const wallet = await Wallet.findByPk(card.walletUUID);

            if(amount > 0 && amount < wallet.balance) {
                try {
                    sequelize
                        .transaction(async (tx) => {

                            card.balance = parseFloat(card.balance) + amount;
                            await card.save();
                            wallet.balance = parseFloat(wallet.balance) - amount;
                            await wallet.save();
                            await Transfer.create({
                                amount,
                                fromUUID: wallet.uuid,
                                fromCurrency: wallet.currency,
                                fromEntityType: 'wallet',
                                toUUID: card.uuid,
                                toCurrency: card.currency,
                                toEntityType: 'card',
                            });

                            return card;
                        })
                        .then(card => {
                            response.status(200).json({status: 'success', data: card.toJSON()});
                        })
                        .catch(error => {
                            response.status(500).json({status: 'error', data: null, message: error});
                        });
                    } catch(error) {
                        response.status(500).json({status: 'error', data: null, message: error});
                    }
            } else if(amount < 0 && amount < card.balance) {

                sequelize
                    .transaction(async (tx) => {
                        card.balance = parseFloat(card.balance) + amount;;
                        card.save();
                        wallet.balance = parseFloat(wallet.balance) - amount;;
                        wallet.save();
                        await Transfer.create({
                            amount: -amount,
                            fromUUID: card.uuid,
                            fromCurrency: card.currency,
                            fromEntityType: 'card',
                            toUUID: wallet.uuid,
                            toCurrency: wallet.currency,
                            toEntityType: 'wallet',
                        });
                        return card;
                    })
                    .then(card => {
                        response.status(200).json({status: 'success', data: card.toJSON()});
                    })
                    .catch(error => {
                        response.status(500).json({status: 'error', data: null, message: error});
                    });
            } else {
                response.status(400).json({status: 'error', data: null, message: 'Not enough funding'});
            }
        })
        .catch(error => {
            // Sending the raw error is may a little too much (and is not secure). 
            // Better send a more appropriate message and log the error.
            response.status(500).json({status: 'error', data: null, message: error});
        });
}

const blockCard = (request, response) => {
    const ownerUUID = request.header('X-User-UUID');
    const cardUUID = request.params.cardUUID;

    if(!ownerUUID) {
        response.status(401).json({status: 'error', data: null, message: 'Not allowed'});
    }

    Card.findByPk(cardUUID)
        .then(async card => {
            if(card.ownerUUID != ownerUUID) {
                response.status(401).json({status: 'error', data: null, message: 'Not allowed'});
            }

            const wallet = await Wallet.findByPk(card.walletUUID);

            if(card.status == 'active') {

                sequelize
                    .transaction(async (tx) => {
                        const amount = parseFloat(card.balance);
                        wallet.balance = parseFloat(wallet.balance) + amount;
                        await wallet.save();
                        card.balance = 0;
                        card.status = 'blocked';
                        await card.save();
                        await Transfer.create({
                            amount,
                            fromUUID: wallet.uuid,
                            fromCurrency: wallet.currency,
                            fromEntityType: 'wallet',
                            toUUID: card.uuid,
                            toCurrency: card.currency,
                            toEntityType: 'card',
                        });
                        return card;
                    })
                    .then(card => {
                        response.status(200).json({status: 'success', data: card.toJSON()});
                    })
                    .catch(error => {
                        response.status(500).json({status: 'error', data: null, message: error});
                    });
            } else if(card.status == 'blocked') {
                card.status = 'active';
                card
                    .save()
                    .then(card => {
                        response.status(200).json({status: 'success', data: card.toJSON()});
                    })
                    .catch(error => {
                        response.status(500).json({status: 'error', data: null, message: error});
                    });
            }
        })
        .catch(error => {
            // Sending the raw error is may a little too much (and is not secure). 
            // Better send a more appropriate message and log the error.
            response.status(500).json({status: 'error', data: null, message: error});
        });
}

const walletTransfer = async (request, response) => {
    const companyUUID = request.header('X-Company-UUID');
    const fromWalletUUID = request.params.walletUUID;
    const toWalletUUID = request.body.toWalletUUID;
    const amount = parseFloat(request.body.amount);

    if(!companyUUID) {
        response.status(401).json({status: 'error', data: null, message: 'Not allowed'});
    }
    if(!fromWalletUUID) {
        response.status(400).json({status: 'error', data: null, message: 'Needs a fromWalletUUID'});
    }
    if(!toWalletUUID) {
        response.status(400).json({status: 'error', data: null, message: 'Needs a toWalletUUID'});
    }
    if(fromWalletUUID == toWalletUUID) {
        response.status(400).json({status: 'error', data: null, message: 'Wrong request'});
    }
    if(!amount) {
        response.status(400).json({status: 'error', data: null, message: 'Needs an amount'});
    }
    if(amount <= 0) {
        response.status(400).json({status: 'error', data: null, message: 'Wrong amount'});
    }
    const fromWallet = await Wallet.findByPk(fromWalletUUID);
    const toWallet = await Wallet.findByPk(toWalletUUID);

    let totalToAmount = amount;
    let fees = 0.0;
    let rate = 1.0;

    if(fromWallet.companyUUID != companyUUID) {
        response.status(401).json({status: 'error', data: null, message: 'Not allowed'});
    }

    if(fromWallet.currency != toWallet.currency) {
        fees = amount * 0.029;

        // TODO: fetch current rate
        rate = await fetchCurrentRate(fromWallet.currency, toWallet.currency);
        totalToAmount = totalToAmount * rate;
    }

    if(fromWallet.amount < amount + fees) {
        response.status(400).json({status: 'error', data: null, message: 'Not enough funding'});
    }

    sequelize
        .transaction(async (tx) => {
            fromWallet.balance = parseFloat(fromWallet.balance) - amount;
            await fromWallet.save();
            toWallet.balance = parseFloat(toWallet.balance) + totalToAmount;
            await toWallet.save();
            await Transfer.create({
                amount,
                fromUUID: fromWallet.uuid,
                fromCurrency: fromWallet.currency,
                fromEntityType: 'wallet',
                toUUID: toWallet.uuid,
                toCurrency: toWallet.currency,
                toEntityType: 'wallet',
                conversionRate: rate,
                conversionFee: fees
            });

            if(fees) {
                const masterWallet = await Wallet.findOne({
                    where: {
                        isMasterWallet:true,
                        currency:fromWallet.currency
                    }
                });

                fromWallet.balance = parseFloat(fromWallet.balance) - fees;
                fromWallet.save();
                masterWallet.balance = parseFloat(masterWallet.balance) + fees;
                masterWallet.save();
                await Transfer.create({
                    amount: fees,
                    fromUUID: fromWallet.uuid,
                    fromCurrency: fromWallet.currency,
                    fromEntityType: 'wallet',
                    toUUID: masterWallet.uuid,
                    toCurrency: masterWallet.currency,
                    toEntityType: 'wallet'
                });
            }

            return fromWallet;
        })
        .then(card => {
            response.status(200).json({status: 'success', data: card.toJSON()});
        })
        .catch(error => {
            response.status(500).json({status: 'error', data: null, message: error});
        });
}


app
    .route('/wallets')
    .get(getWallets)
    .post(addWallet);

app
    .route('/cards')
    .get(getCards)
    .post(addCard);

app.route('/cards/:cardUUID/fund').patch(fundCard);
app.route('/cards/:cardUUID/block').patch(blockCard);
app.route('/wallets/:walletUUID/send').post(walletTransfer);

// Start server
app.listen(process.env.PORT || 3002, () => {
    console.log(`Server listening`);
});